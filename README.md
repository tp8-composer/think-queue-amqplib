# think-queue-amqplib

### 介绍
amqplib 队列驱动

### 相关插件
~~~
https://github.com/rabbitmq/rabbitmq-delayed-message-exchange
解压到/usr/lib/rabbitmq/lib/rabbitmq_server-3.12.4/plugins
安装延时插件
cd /usr/lib/rabbitmq/lib/rabbitmq_server-3.12.4/plugins
sudo rabbitmq-plugins enable rabbitmq_delayed_message_exchange
查看插件配置
sudo rabbitmq-plugins directories -s
查询插件安装状态
sudo rabbitmq-plugins list
~~~